# Compiling Expressions to Stack machine.

[![Build Status][build-status-img]][build-status]

This repository contains the source code of a simple but illustrative
example of a compiler: The source language is the language of
arithmetic expressions over integers with operators `+`, `-` and
`*`. The expression compiler `ec` compiles a list of expressions given
one per line to an equivalent _assembly program_ for a simple stack
machine. The stack machine has the following instruction set.

- `push i`: pushes the integer `i` on to the stack

- `plus`, `minus`, `mul` : Pops the top two elements of the stack and
  pushes the result of adding, subtracting, or multiplying
  respectively onto the top of the stack.

- `clear` : clear the stack
- `print` : print the top of the stack
- `stack` : print the entire stack.

The compiler is organised as library `stackmachine` ([ODoc API
documentation][doc]) which is then used by a driver program `ec` to
perform the actual compilation.

## How to read the source code ?

This repository is meant show the skeleton The expression compiler
`ec` performs a direct translation between the expression language to
the instructions for the stack machine. We suggest the following
reading order for the source code.

1. The ASTs of the source and target language are captured by the
   types [`Ast.expr`][ast] and [Machine.program][machine] respectively
   and are available inside the files [ast.ml][ast] and
   [machine.ml][machine]. Read these files first to understand how
   one captures the ASTs using Ocaml's variant types.

2. The compiler is thus captured by the SML function `compile :
   Ast.pxpr -> Machine.program` and is given in
   [translate.ml][translate] file.

3. The front end is written using tools like [`menhir`][menhir] and
   [`ocamllex`][ocamllex] which generates ocaml modules for the parser
   and lexer given in their specification files [`expr.mly`][expr.mly]
   and [`expr.mll`][expr.mll]


The suggested reading order is therefore [`ast.ml`][ast],
[`machine.ml`][machine], and [`translate.ml`][translate]. The actual
input to the [`menhir`][menhir] and [`ocamllex`][ocamllex] tool is
files [`parser.mly`][parser.mly] and [`lexer.mll`][lexer.mll], which you can
skip in the first reading.

[ast]: <lib/ast.ml>
[machine]: <lib/machine.ml>
[translate]: <lib/translate.ml>
[parser.mly]: <lib/parser.mly>
[lexer.mll]: <lib/lexer.mll>
[menhir]: <http://gallium.inria.fr/~fpottier/menhir/manual.html>
[ocamllex]: <https://ocaml.org/manual/lexyacc.html>
[book]: <https://www.cs.princeton.edu/~appel/modern/> "Modern Compiler Implementation"
[mips]: <https://en.wikipedia.org/wiki/MIPS_architecture>
[build-status-img]: <https://gitlab.com/piyush-kurur/stackmachine-ocaml/badges/main/pipeline.svg> "Build Status"
[build-status]: <https://gitlab.com/piyush-kurur/stackmachine-ocaml/pipelines/latest> "Build Status"

[doc]: <https://gitlab.com/piyush-kurur/stackmachine-ocaml/-/jobs/artifacts/main/file/_build/default/_doc/_html/stackmachine/index.html?job=build>
